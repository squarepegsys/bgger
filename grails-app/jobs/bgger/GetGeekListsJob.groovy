package bgger

import groovy.sql.Sql

/**
 * Created by mikeh on 9/15/15.
 */
class GetGeekListsJob {
    static triggers = {
        cron name: 'GetGeekListTrigger',
                cronExpression: "0 0 * * * ?"
    }

    def discoverGeekListService
    def bggFetcherService
    def dataSource

    def execute() {

        discoverGeekListService.parseAndUpdateLists("https://boardgamegeek.com/rss/subscriptions/userid/361910/4355a8f194ff5d733cc6a35d7ed5aad5/group/geeklist")

        // are there more?

        /*
        def newGL = "select bgg_id from geek_list gl \n" +
                "where not exists (\n" +
                "select 1 from geek_list_item gli where\n" +
                "gli.geeklist_id=gl.id) "

        log.error("finding old GLs")

        Sql.newInstance(dataSource).eachRow(newGL) {
            row->
                log.error("finding item for list ${row["bgg_id"]}")
                bggFetcherService.findGeekList(row["bgg_id"] as String)
                sleep(10000)


        }*/
    }
}
